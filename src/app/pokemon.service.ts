import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { API_Response } from './API_Response';
import { Pokemon } from './pokemon';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  cachedPokemons: Pokemon[] = [];
  private nextTwentyPokemonsUrl = '' 
  private base_url = 'https://pokeapi.co/api/v2/pokemon';


  constructor(private http: HttpClient) { }

  fetchFirstTwentyPokemons (): Observable<API_Response> {
    return this.http.get<API_Response>(this.base_url);
  }

  fetchPokemonByName(name: string): Observable<Pokemon> {
    const url = `${this.base_url}/${name}`;
    return this.http.get<Pokemon>(url);
  }

  fetchNextTwentyPokemons(url: string): Observable<API_Response> {
    return this.http.get<API_Response>(url);
  }





  getCachedPokemons (){
    return this.cachedPokemons;
  }

  setCachedPokemons(pokemons){
    this.cachedPokemons = pokemons;
  }  

  getNextTwentyPokemonsUrl() {
    return this.nextTwentyPokemonsUrl;
  }

  setNextTwentyPokemonsUrl(url) {
    this.nextTwentyPokemonsUrl = url;
  }

  noCachedPokemons() {
    return this.cachedPokemons.length == 0
  }


}
