import { Component, OnInit, Input } from '@angular/core';
import { Pokemon } from '../pokemon';


@Component({
  selector: 'app-pokemon-stats',
  templateUrl: './pokemon-stats.component.html',
  styleUrls: ['./pokemon-stats.component.css']
})
export class PokemonStatsComponent implements OnInit {
  @Input()  pokemon: Pokemon;
  constructor() { }

  ngOnInit() {
  }

}
