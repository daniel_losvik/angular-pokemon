import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PokemonListComponent} from "./pokemon-list/pokemon-list.component"
import {PokemonDetailComponent} from "./pokemon-detail/pokemon-detail.component"


const routes: Routes = [
  { path: '', component: PokemonListComponent},
  { path: 'pokemonlist', component: PokemonListComponent },
  { path: 'detail/:name', component: PokemonDetailComponent }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
