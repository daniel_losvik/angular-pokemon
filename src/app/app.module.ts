import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PokemonListComponent } from './pokemon-list/pokemon-list.component';
import { HttpClientModule } from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { PokemonDetailComponent } from './pokemon-detail/pokemon-detail.component';
import { PokemonProfileComponent } from './pokemon-profile/pokemon-profile.component';
import { PokemonStatsComponent } from './pokemon-stats/pokemon-stats.component';
import { PokemonMovesComponent } from './pokemon-moves/pokemon-moves.component';


@NgModule({
  declarations: [
    AppComponent,
    PokemonListComponent,
    PokemonDetailComponent,
    PokemonProfileComponent,
    PokemonStatsComponent,
    PokemonMovesComponent,
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
