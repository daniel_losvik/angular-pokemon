export class Pokemon {
    id: number;
    name: string;
    height: number;
    weight: number;
    moves: Array<{move}>;
    abilities: Array<{ability}>;
  }