import { Component, OnInit } from "@angular/core";
import { Pokemon } from "../pokemon";
import { PokemonService } from "../pokemon.service";

@Component({
  selector: "app-pokemon-list",
  templateUrl: "./pokemon-list.component.html",
  styleUrls: ["./pokemon-list.component.css"]
})
export class PokemonListComponent implements OnInit {
  pokemons: Pokemon[] = [];
  nextTwentyPokemonsUrl: string = "";
  searchString: string = "";
  displayedAndFilteredPokemons: Pokemon[] = [];


  constructor(
    private pokemonService: PokemonService,
  ) {}

  ngOnInit() {
    window.scrollTo(0, 0)
    
    if (this.pokemonService.noCachedPokemons()) {
      this.getFirstTwentyPokemons();
    } 
    else {
      this.pokemonService.getCachedPokemons().forEach(pokemon => {
        this.pokemons.push(pokemon);
        this.filterPokemonsFromSearchString();
      });
      this.nextTwentyPokemonsUrl = this.pokemonService.getNextTwentyPokemonsUrl();
    }
  }

  getFirstTwentyPokemons(): void {
    this.pokemonService.fetchFirstTwentyPokemons().subscribe(response => {
      this.nextTwentyPokemonsUrl = response.next;
      response.results.forEach((pokemon: Pokemon) => {
        this.pokemonService.fetchPokemonByName(pokemon.name).subscribe(pokemon => {
          this.pokemons.push(pokemon);
          this.filterPokemonsFromSearchString();
        });
      });
    });
  }

  loadMorePokemons(url: string): void {
    if(this.nextTwentyPokemonsUrl) {
      this.pokemonService.fetchNextTwentyPokemons(this.nextTwentyPokemonsUrl).subscribe(response => {
        this.nextTwentyPokemonsUrl = response.next;
          response.results.forEach((element: Pokemon) => {
            this.pokemonService.fetchPokemonByName(element.name).subscribe(pokemon => {
              this.pokemons.push(pokemon);
              this.filterPokemonsFromSearchString();
            });
          });
      });
    }
  }

  filterPokemonsFromSearchString(): void {
    this.displayedAndFilteredPokemons = this.pokemons.filter(pokemon => {
      return pokemon.name
        .toLowerCase()
        .includes(this.searchString.toLowerCase());
    });
    this.displayedAndFilteredPokemons.sort((a,b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
  }
  
  saveLoadedPokemonsBeforeNavigating() {
    this.pokemonService.setCachedPokemons(this.pokemons);
    this.pokemonService.setNextTwentyPokemonsUrl(this.nextTwentyPokemonsUrl);
  }
  
}
