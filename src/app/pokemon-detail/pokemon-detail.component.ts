import { Component, OnInit, Input } from '@angular/core';
import {Location} from "@angular/common";
import { Pokemon } from '../pokemon';
import { PokemonService } from "../pokemon.service";
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.css']
})
export class PokemonDetailComponent implements OnInit {
 @Input() pokemon: Pokemon
  onClickBack(){
    this._location.back();
  }
  constructor(private _location: Location, private pokemonService: PokemonService, private route: ActivatedRoute)  { }

  ngOnInit() {
    const name = this.route.snapshot.paramMap.get('name');


    this.pokemonService.fetchPokemonByName(name).subscribe(pokemon => {
      this.pokemon = pokemon;
      this.pokemon.height = this.pokemon.height / 10; 
      this.pokemon.weight = this.pokemon.weight / 10; 
      this.pokemon.moves.forEach(move => {
        move.move.name = move.move.name.replace(/-/g,' ');
      });
      this.pokemon.abilities.forEach(ability => {
        ability.ability.name = ability.ability.name.replace(/-/g,' ');
      });
    })

    }
  }


